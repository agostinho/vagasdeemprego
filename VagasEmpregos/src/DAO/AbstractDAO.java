package DAO;

import java.util.List;

import DB.Conexao;
import Model.Endereco;

public abstract class AbstractDAO <T> {
	
	public abstract void salvar(T object);
	
	public abstract void atualizar(T object);
	
	public  abstract void deletar(T object);
	
	public  abstract void pesquisar(T object);
	
	public abstract List<T> listar();

}
