package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Empresa;
import Model.Endereco;
import Model.Recrutador;

public class RecrutadorDAO extends AbstractDAO<Recrutador>{


	public void salvar(Recrutador stq) {
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "insert into Recrutador (nome,cargo,telefone,email)" + "values (?,?,?,?)";
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setString(1, stq.getNome());
			stmt.setString(2, stq.getCargo());
			stmt.setInt(3, stq.getTelefone());
			stmt.setString(4, stq.getEmail());
			
			
			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel inserir Recrutador");

		}
	}

	public void atualizar(Recrutador stq) {
		String sql = ("update Recrutador set nome = ?,cargo = ?,telefone = ?,email = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			
			stmt.setString(1, stq.getNome());
			stmt.setString(2, stq.getCargo());
			stmt.setInt(3, stq.getTelefone());
			stmt.setString(4, stq.getEmail());
			
			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel alterar o Recrutador");

		}
		
	}


	public void deletar(Recrutador stq) {
		String sql = (" delete from  Recrutador where nome = ? ");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setString(1, stq.getNome());

			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel excluir o Recrutador");

		}
	}


	public void pesquisar(Recrutador stq) {
		
		String sql = ("select * from Recrutador where data = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setString(1, stq.getNome());
			ResultSet rst = stmt.executeQuery();

			stmt.execute();
			rst.next();

			Recrutador t = new Recrutador();
			t.setNome(rst.getString(1));
			t.setCargo(rst.getString(2));
			t.setTelefone(rst.getInt(3));
			t.setEmail(rst.getString(4));
	
		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel pesquisar o Recrutador");	
	}
	}

	public List<Recrutador> listar() {
		List<Recrutador> recrutador  = new ArrayList<>();
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "select * from Recrutador";
			PreparedStatement stmt = conexao.prepareStatement(sql);
			ResultSet rst = stmt.executeQuery();

			while (rst.next()) {
				Recrutador stq = new Recrutador();

				String nome= rst.getString("nome");
				String cargo= rst.getString("cargo");
				int telefone = rst.getInt("");
				String email= rst.getString("email");
				
				stq.setNome(nome);
				stq.setCargo(cargo);
				stq.setTelefone(telefone);
				stq.setEmail(email);
			
				System.out.println("Nome: " + nome);
				System.out.println("cargo: " + cargo);
				System.out.println("telefone: " + telefone);
				System.out.println("email: " + email);
			}

			rst.close();
			stmt.close();
			conexao.close();
			return recrutador;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
