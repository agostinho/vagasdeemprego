package DAO;

public class FactoryDAO {
	
	public static AbstractDAO getDAO(String dao) {
        switch (dao) {
            case "model.Empresa":
                return new EmpresaDAO();
            case "model.Endereco":
                return new EnderecoDAO();
            case "model.Recrutador":
                return new RecrutadorDAO();
            case "model.Vaga":
                return new VagaDAO();
        }
        return null;
    }
}
