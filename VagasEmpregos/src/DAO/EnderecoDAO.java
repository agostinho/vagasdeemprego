package DAO;

import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DB.Conexao;
import Model.Endereco;

public class EnderecoDAO extends AbstractDAO<Endereco> {

	public void salvar(Endereco stq){
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "insert into Endereco (rua,cep,numero,estado,cidade,bairro)" + "values (?,?,?,?,?,?)";
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setString(1, stq.getRua());
			stmt.setInt(2, stq.getCEP());
			stmt.setInt(3,stq.getNumero());
			stmt.setString(4, stq.getEstado());
			stmt.setString(5, stq.getCidade());
			stmt.setString(6, stq.getBairro());
			
			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel inserir Endereco");

		}
	}

	public void atualizar(Endereco stq) {
		
		String sql = ("update Endereco set rua = ?,CEP = ?,numero = ?,estado = ?,cidade = ?,bairro = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			
			stmt.setString(1, stq.getRua());
			stmt.setInt(2, stq.getCEP());
			stmt.setInt(3,stq.getNumero());
			stmt.setString(4, stq.getEstado());
			stmt.setString(5, stq.getCidade());
			stmt.setString(6, stq.getBairro());

			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel alterar o Endereco");

		}
		
	}


	public void deletar(Endereco stq) {
		
		String sql = (" delete from  Endereco where CEP = ? ");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setInt(2, stq.getCEP());

			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel excluir do Endereco");

		}
		
	}


	public void pesquisar(Endereco stq) {
		
		String sql = ("select * from Endereco where data = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setInt(2, stq.getCEP());
			ResultSet rst = stmt.executeQuery();

			stmt.execute();
			rst.next();

			Endereco t = new Endereco();
			t.setRua(rst.getString(1));
			t.setCEP(rst.getInt(1));
			t.setNumero(rst.getInt(1));
			t.setEstado(rst.getString(1));
			t.setCidade(rst.getString(1));
			t.setBairro(rst.getString(1));
	
		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel pesquisar no Estoque");	
	}
	}


	public List<Endereco> listar() {
		List<Endereco> t  = new ArrayList<>();
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "select * from Endereco";
			PreparedStatement stmt = conexao.prepareStatement(sql);
			ResultSet rst = stmt.executeQuery();

			while (rst.next()) {
				Endereco stq = new Endereco();

				String rua= rst.getString("rua");
				int CEP = rst.getInt("CEP");
				int numero= rst.getInt("numero");
				String estado= rst.getString("estado");
				String cidade= rst.getString("cidade");
				String bairro= rst.getString("bairro");
				
				stq.setRua(rua);
				stq.setCEP(CEP);
				stq.setNumero(numero);
				stq.setEstado(estado);
				stq.setCidade(cidade);
				stq.setBairro(bairro);
				

				System.out.println("Rua: " + rua);
				System.out.println("CEP: " + CEP);
				System.out.println("Numero: " + numero);
				System.out.println("Estado: " + numero);
				System.out.println("Cidade: " + cidade);
				System.out.println("Bairro: " + bairro);
				
			}

			rst.close();
			stmt.close();
			conexao.close();
			return t;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

}
