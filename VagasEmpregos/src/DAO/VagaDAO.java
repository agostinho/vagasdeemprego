package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Recrutador;
import Model.Vaga;

public class VagaDAO extends AbstractDAO<Vaga> {

	
	public void salvar(Vaga stq) {
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "insert into Vaga (salario,beneficios,cargaHoraria,cargo,situa��o)" + "values (?,?,?,?,?)";
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setFloat(1, stq.getSalario());
			stmt.setString(2, stq.getBeneficios());
			stmt.setString(3, stq.getCargaHoraria());
			stmt.setString(4, stq.getCargo());
			stmt.setString(5, stq.getSitua��o());
			
			
			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel inserir Vaga");

		}
	}


	public void atualizar(Vaga stq) {
		String sql = ("update Vaga set salario = ?,beneficios = ?,cargaHoraria = ?,cargo = ?,situa��o = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setFloat(1, stq.getSalario());
			stmt.setString(2, stq.getBeneficios());
			stmt.setString(3, stq.getCargaHoraria());
			stmt.setString(4, stq.getCargo());
			stmt.setString(5, stq.getSitua��o());
			
			
			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel alterar a Vaga");

		}
	}


	public void deletar(Vaga stq) {
		String sql = (" delete from  Vaga where nome = ? ");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setString(4, stq.getCargo());

			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel excluir a Vaga");

		}
	}


	public void pesquisar(Vaga stq) {

		String sql = ("select * from Vaga where data = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setString(4, stq.getCargo());
			ResultSet rst = stmt.executeQuery();

			stmt.execute();
			rst.next();

			Vaga t = new Vaga();
			t.setSalario(rst.getFloat(1));
			t.setBeneficios(rst.getString(2));
			t.setCargaHoraria(rst.getString(3));
			t.setCargo(rst.getString(4));
			t.setSitua��o(rst.getString(5));
	
		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel pesquisar o Recrutador");	
	}
	}


	public List<Vaga> listar() {
		List<Vaga> vaga  = new ArrayList<>();
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "select * from Vaga";
			PreparedStatement stmt = conexao.prepareStatement(sql);
			ResultSet rst = stmt.executeQuery();

			while (rst.next()) {
				Vaga stq = new Vaga();

				Float salario= rst.getFloat("salario");
				String beneficios= rst.getString("beneficios");
				String cargaHoraria= rst.getString("cargaHoraria");
				String cargo= rst.getString("cargo");
				String situa��o= rst.getString("situa��o");
				
				stq.setSalario(salario);
				stq.setBeneficios(beneficios);
				stq.setCargaHoraria(cargaHoraria);
				stq.setCargo(cargo);
				stq.setSitua��o(situa��o);
			
				System.out.println("salario: " + salario);
				System.out.println("beneficios: " + beneficios);
				System.out.println("cargaHoraria: " + cargaHoraria);
				System.out.println("cargo: " + cargo);
				System.out.println("situa��o: " + situa��o);
			}

			rst.close();
			stmt.close();
			conexao.close();
			return vaga;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


}
