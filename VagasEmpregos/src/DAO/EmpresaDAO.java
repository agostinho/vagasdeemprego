package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Model.Empresa;
import Model.Endereco;

public  class EmpresaDAO extends AbstractDAO<Empresa> {


	public void salvar(Empresa stq) {
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "insert into Empresa (nome,CNPJ,endereco,ramo,email)" + "values (?,?,?,?,?)";
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setString(1, stq.getNome());
			stmt.setDouble(2, stq.getCNPJ());
			stmt.setObject(3, stq.getEndereco());
			stmt.setString(4, stq.getRamo());
			
			
			
			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel inserir Empresa");

		}
	}


	public void atualizar(Empresa stq) {
		String sql = ("update Empresa set nome = ?,CNPJ = ?,endereco = ?,ramo = ?,email = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			
			stmt.setString(1, stq.getNome());
			stmt.setDouble(2, stq.getCNPJ());
			stmt.setObject(3, stq.getEndereco());
			stmt.setString(4, stq.getRamo());
			
			
			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel alterar a Empresa");

		}
		
	}

	public void deletar(Empresa stq) {
		String sql = (" delete from  Empresa where CNPJ = ? ");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);

			stmt.setDouble(2, stq.getCNPJ());

			stmt.execute();
			stmt.close();
			conexao.close();

		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel excluir a Empresa");

		}
	}

	public void pesquisar(Empresa stq) {
		
		String sql = ("select * from Empresa where data = ?");
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			PreparedStatement stmt = conexao.prepareStatement(sql);
			stmt.setDouble(2, stq.getCNPJ());
			ResultSet rst = stmt.executeQuery();

			stmt.execute();
			rst.next();

			Empresa t = new Empresa();
			t.setNome(rst.getString(1));
			t.setCNPJ(rst.getDouble(1));
			t.setEndereco(rst.getString(1));
			t.setRamo(rst.getString(1));
		
	
		} catch (SQLException e) {
			System.out.println("N�o foi poss�vel pesquisar a Empresa");	
	}
	}
	
	public List<Empresa> listar() {
		List<Empresa> t  = new ArrayList<>();
		try {
			Connection conexao = BDConexao.getConnectionPostgres();
			String sql = "select * from Endereco";
			PreparedStatement stmt = conexao.prepareStatement(sql);
			ResultSet rst = stmt.executeQuery();

			while (rst.next()) {
				Endereco stq = new Endereco();

				String rua= rst.getString("rua");
				int CEP = rst.getInt("CEP");
				int numero= rst.getInt("numero");
				String estado= rst.getString("estado");
				String cidade= rst.getString("cidade");
				String bairro= rst.getString("bairro");
				
				stq.setRua(rua);
				stq.setCEP(CEP);
				stq.setNumero(numero);
				stq.setEstado(estado);
				stq.setCidade(cidade);
				stq.setBairro(bairro);
				

				System.out.println("Rua: " + rua);
				System.out.println("CEP: " + CEP);
				System.out.println("Numero: " + numero);
				System.out.println("Estado: " + numero);
				System.out.println("Cidade: " + cidade);
				System.out.println("Bairro: " + bairro);
				
			}

			rst.close();
			stmt.close();
			conexao.close();
			
			return t;
			
		} catch (SQLException e) {
			e.printStackTrace();
		return null;
		}
	}



	}


