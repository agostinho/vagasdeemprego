package View;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import DAO.EmpresaDAO;
import Model.Empresa;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTable;

public class CadastroEmpresa extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textNome;
	private JTextField textCEP;
	private JLabel lblRua;
	private JTextField textRua;
	private JLabel lblBairro;
	private JTextField textBairro;
	private JLabel lblCidade;
	private JTextField textCidade;
	private JLabel lblNumero;
	private JTextField textNumero;
	private JLabel lblTelefone;
	private JTextField textTelefone;
	private JLabel lblRamo;
	private JTextField textRamo;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CadastroEmpresa frame = new CadastroEmpresa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CadastroEmpresa() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 579, 437);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblCNPJ = new JLabel("* CNPJ");
		lblCNPJ.setBounds(36, 25, 83, 14);
		lblCNPJ.setVerticalAlignment(SwingConstants.TOP);
		lblCNPJ.setForeground(new Color(0, 0, 0));
		lblCNPJ.setFont(new Font("Tahoma", Font.PLAIN, 11));
		contentPane.add(lblCNPJ);
		
		textField = new JTextField();
		textField.setBounds(95, 22, 146, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNome = new JLabel("* Nome");
		lblNome.setBounds(36, 50, 46, 14);
		contentPane.add(lblNome);
		
		textNome = new JTextField();
		textNome.setBounds(95, 47, 146, 20);
		contentPane.add(textNome);
		textNome.setColumns(10);
		
		JLabel lblCep = new JLabel("* CEP");
		lblCep.setBounds(36, 91, 46, 14);
		contentPane.add(lblCep);
		
		textCEP = new JTextField();
		textCEP.setBounds(95, 88, 86, 20);
		contentPane.add(textCEP);
		textCEP.setColumns(10);
		
		lblRua = new JLabel("* Rua");
		lblRua.setBounds(195, 91, 46, 14);
		contentPane.add(lblRua);
		
		textRua = new JTextField();
		textRua.setBounds(261, 88, 86, 20);
		contentPane.add(textRua);
		textRua.setColumns(10);
		
		lblBairro = new JLabel("* Bairro");
		lblBairro.setBounds(36, 129, 46, 14);
		contentPane.add(lblBairro);
		
		textBairro = new JTextField();
		textBairro.setBounds(95, 119, 86, 20);
		contentPane.add(textBairro);
		textBairro.setColumns(10);
		
		lblCidade = new JLabel("* Cidade");
		lblCidade.setBounds(195, 129, 46, 14);
		contentPane.add(lblCidade);
		
		textCidade = new JTextField();
		textCidade.setBounds(261, 126, 86, 20);
		contentPane.add(textCidade);
		textCidade.setColumns(10);
		
		lblNumero = new JLabel("* Numero");
		lblNumero.setBounds(36, 171, 46, 14);
		contentPane.add(lblNumero);
		
		textNumero = new JTextField();
		textNumero.setBounds(95, 168, 86, 20);
		contentPane.add(textNumero);
		textNumero.setColumns(10);
		
		lblTelefone = new JLabel("* Telefone");
		lblTelefone.setBounds(195, 171, 56, 14);
		contentPane.add(lblTelefone);
		
		textTelefone = new JTextField();
		textTelefone.setBounds(263, 168, 86, 20);
		contentPane.add(textTelefone);
		textTelefone.setColumns(10);
		
		lblRamo = new JLabel("* Ramo");
		lblRamo.setBounds(36, 213, 46, 14);
		contentPane.add(lblRamo);
		
		textRamo = new JTextField();
		textRamo.setBounds(95, 210, 86, 20);
		contentPane.add(textRamo);
		textRamo.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Empresa empresa = new Empresa();
				empresa.setCNPJ(Double.parseDouble(textField.getText()));
				empresa.setNome(textNome.getText());
				empresa.setEndereco(textCEP.getText());
				empresa.setEndereco(textRua.getText());
				empresa.setEndereco(textBairro.getText());
				empresa.setEndereco(textCidade.getText());
				empresa.setEndereco(textNumero.getText());
				empresa.setEndereco(textTelefone.getText());
				empresa.setEndereco(textRamo.getText());
				
				if (textField.getText().isEmpty() || textNome.getText().isEmpty() || textCEP.getText().isEmpty()
						|| textRua.getText().isEmpty() || textBairro.getText().isEmpty() || textCidade.getText().isEmpty() 
						|| textNumero.getText().isEmpty() || textTelefone.getText().isEmpty() || textRamo.getText().isEmpty() ) {
					JOptionPane.showMessageDialog(null, "Os campos n�o podem retornar vazios");

				}
				EmpresaDAO empresadao = new EmpresaDAO();
				empresadao.salvar(empresa);
				
			}
		});
	
		btnCadastrar.setBounds(212, 209, 135, 23);
		contentPane.add(btnCadastrar);
		
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setBounds(10, 256, 543, 133);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setBackground(new Color(255, 255, 255));
		table.setBounds(36, 373, 475, -101);
		contentPane.add(table);
		


	}
}
