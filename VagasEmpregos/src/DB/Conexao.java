package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

	public static Connection getConnectionPostgres() throws SQLException {
		try {
			/* Class.forName("org.postgresql.Driver"); */
			Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstoredb", "root",
					"root");
			return connection;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}