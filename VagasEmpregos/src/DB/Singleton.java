package DB;

public class Singleton {
	
	private static Singleton instancia;
	
	private Conexao con;
	
	
	private Singleton(){
	con = new Conexao();
	}
	
	public static synchronized Singleton getInstance(){
		if(instancia == null){
				instancia = new Singleton();
			}
			return instancia;
		}
	
	public Conexao getCon() {
		return con;
	}
	public void setCon(Conexao con) {
		this.con = con;
	}
	}
