package Adapter;

public class Principal {

	
	public static  void  main (String []args){
		 Pato pato = new PatoSelvagem();
		 testarPato(pato);
		 
		 Peru peru = new PeruSelvagem();
		 Pato peruAdapter =  new PeruAdapter(peru);
		 testarPato(peruAdapter);
		 
	}
	public static void  testarPato(Pato pato){
		
		pato.grasnar();
		pato.voar();
	}
}
