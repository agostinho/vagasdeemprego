package Model;

public class Empresa {
	
	private int id;
	private String nome;
	private Double CNPJ;
	private String endereco;
	private String ramo;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Double getCNPJ() {
		return CNPJ;
	}
	public void setCNPJ(Double CNPJ) {
		CNPJ = CNPJ;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getRamo() {
		return ramo;
	}
	public void setRamo(String ramo) {
		this.ramo = ramo;
	}

	}
	
	
	


