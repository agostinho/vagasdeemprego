package Model;

public class Vaga {
	
	private int id;
	private float salario;
	private String beneficios;	
	private String cargaHoraria;
	private String cargo;
	private String situa��o;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getSalario() {
		return salario;
	}
	public void setSalario(float salario) {
		this.salario = salario;
	}
	public String getBeneficios() {
		return beneficios;
	}
	public void setBeneficios(String beneficios) {
		this.beneficios = beneficios;
	}
	public String getCargaHoraria() {
		return cargaHoraria;
	}
	public void setCargaHoraria(String cargaHoraria) {
		this.cargaHoraria = cargaHoraria;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public String getSitua��o() {
		return situa��o;
	}
	public void setSitua��o(String situa��o) {
		this.situa��o = situa��o;
	}
	

}
