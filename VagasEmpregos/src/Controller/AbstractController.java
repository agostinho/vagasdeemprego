package Controller;

import java.util.List;

public abstract class AbstractController <T> {
	
	public abstract void salvar(T object);
	
	public abstract void atualizar(T object);
	
	public  abstract void deletar(T object);
	
	public  abstract void pesquisar(T object);
	
	public abstract List<T> listar();
	
	public abstract boolean validate(T object) ;

}
