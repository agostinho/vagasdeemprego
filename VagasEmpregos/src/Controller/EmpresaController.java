package Controller;

import java.util.List;

import DAO.AbstractDAO;
import DAO.FactoryDAO;
import Model.Empresa;

public class EmpresaController extends AbstractController<Empresa> {


	public void salvar(Empresa object) {
			if(validate(object)){
            	AbstractDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.salvar(object);
            }
	}


	@Override
	public void atualizar(Empresa object) {
				if(validate(object)){
	            	AbstractDAO dao = FactoryDAO.getDAO(object.getClass().getName());
	                dao.atualizar(object);
	            }
	       
	    }
	

	@Override
	public void deletar(Empresa object) {
			if(validate(object)){
				AbstractDAO dao = FactoryDAO.getDAO(object.getClass().getName());
                dao.deletar(object);
            }
      }

	@Override
	public void pesquisar(Empresa object) {
				if(validate(object)){
					AbstractDAO dao = FactoryDAO.getDAO(object.getClass().getName());
	                dao.pesquisar(object);
	            }
	     }

	@Override
	public List<Empresa> listar() {
		 Empresa empresa = new Empresa();
	        AbstractDAO dao = FactoryDAO.getDAO(empresa.getClass().getName());
	        return dao.listar();
		
	}

	@Override
	public boolean validate(Empresa object)  {
		String msg_erro = "";
        if(object == null)
            System.out.println("A Empresa n�o � um objeto v�lido\n");
        
            msg_erro += "O Nome da empresa deve estar preenchido\n";
        if(new Double(object.getCNPJ()) == null)
            msg_erro += "O CNPJ da Empresa deve estar preenchido\n";
        if(object.getEndereco()== null)
            msg_erro += "O Endere�o da Empresa deve estar preenchido corretamente\n";
        if(object.getRamo()==null)
        	msg_erro += " O Ramo da empresa deve ser preenchido corretamente\n";
       
        if(!msg_erro.isEmpty())
             return false;
										return false;
	}

}
