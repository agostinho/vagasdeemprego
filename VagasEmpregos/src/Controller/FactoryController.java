package Controller;


public class FactoryController {
	public static AbstractController getDAO(String dao) {
        switch (dao) {
            case "model.Empresa":
                return new EmpresaController();
            case "model.Endereco":
                return new EnderecoController();
            case "model.Recrutador":
                return new RecrutadorController();
            case "model.Vaga":
                return new VagaController();
        }
        return null;
    }
}
